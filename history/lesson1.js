'use strict';

function ClassStudent (firstname = 'First name', lastname = 'Last name', birth = '1970-01-01' ) {
    this.firstName = firstname;
    this.lastName = lastname;
    this.birthDate = birth;
    this.Scores = [50,60,70,80];
    this.Attendance = new Array(25);
};

ClassStudent.prototype.getBirthDate = function(){
    return this.birthDate;
};
ClassStudent.prototype.getAverageScore = function(){
    if (this.Scores.length === 0) {
        return 0;
    } else {
        return this.Scores.reduce((a, b) => {
            return a + b;
        }, 0) / this.Scores.length;
    }
};
ClassStudent.prototype.setScore = function(score = 0){
    this.Scores.push(score);
};
ClassStudent.prototype.countAttendance = function() {
    return this.Attendance.filter((item) => {
        return item === true;
    }).length;
};
ClassStudent.prototype.getAverageAttendance = function(){
    var countLessons = this.Attendance.filter((item) => {
        return item !== undefined;
    }).length;

    if (countLessons === 0) {
        return 0;
    } else {
        return this.countAttendance()/countLessons;
    }
};
ClassStudent.prototype.present = function(){
    this.Attendance.shift();
    this.Attendance.push(true);
};
ClassStudent.prototype.absent = function(){
    this.Attendance.shift();
    this.Attendance.push(false);
};
ClassStudent.prototype.summary = function(){
    var averageScore = this.getAverageScore();
    var averageAttendance = this.getAverageAttendance();
    if (averageAttendance === 0) {
        console.log('Сначала нужно начать');
    } else if (this.Scores.length === 0) {
        console.log('Заработать бы одну оценку...');
    } else if (averageScore > 90 && averageAttendance > 0.9) {
        console.log('Ути какой молодчинка!');
    } else if ((averageScore > 90 && averageAttendance < 0.9) || (averageScore < 90 && averageAttendance > 0.9)) {
        console.log('Норм, но можно лучше');
    } else {
        console.log('Редиска!');
    }
};

function ClassGroup() {
    this.students = [];
};
ClassGroup.prototype.newStudent = function(firstname, lastname, birthdate){
    var student = new ClassStudent(firstname, lastname, birthdate);
    this.students.push(student);
};
ClassGroup.prototype.addStudent = function(student){
    if (student.__proto__.constructor.name !== 'ClassStudent') {
        console.log('Какой-то не такой студент');
    } else {
        this.students.push(student);
    }
};
ClassGroup.prototype.createRaiting = function(){
    return this.students.map((student) => {
        return {
            lastName: student.lastName
            , count: student.countAttendance()
            , scores: student.Scores
            , score: student.getAverageScore()
        };
    });
};
ClassGroup.prototype.attendance = function(studentName){
    if (studentName) {
        let raiting = this.createRaiting().sort((a, b) => {
            return b.count - a.count;
        });
        var studentIndex = raiting.findIndex((student) => {
            return student.lastName === studentName;
        });
        if (studentIndex < 0) {
            console.log('Нет такого студента');
        } else {
            console.log('Рейтинг студента ', studentName, ' по посещаемости: ', studentIndex);
        }
    } else {
        if (this.students.length === 0) {
            console.log('Упс, нет студентов');
        } else {
            let avg = this.students.reduce((count, student) => {
                return count + student.getAverageAttendance();
            }, 0) / this.students.length;
            console.log('Средняя посещаемость: ', avg);
        }
    }
};
ClassGroup.prototype.perfomance = function(studentName){
    if (studentName) {
        let raiting = this.createRaiting().sort((a, b) => {
            return b.score - a.score;
        });
        var studentIndex = raiting.findIndex((student) => {
            return student.lastName === studentName;
        });
        if (studentIndex < 0) {
            console.log('Нет такого студента');
        } else {
            console.log('Рейтинг студента ', studentName, ' по оценкам: ', studentIndex, ' и балы: ', rating[studentIndex].Scores);
        }
    } else {
        if (this.students.length === 0) {
            console.log('Упс, нет студентов');
        } else {
            let avg = this.students.reduce((count, student) => {
                return count + student.getAverageScore();
            }, 0) / this.students.length;
            console.log('Среняя успеваемость: ', avg);
        }
    }
};
