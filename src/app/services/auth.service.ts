import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

interface LoginData {
  message: string;
}

@Injectable()
export class AuthService {
  public url: string;
  public isAuthorize = false;
  public errorMessage = 'Login error';

  constructor(private http: HttpClient) { }

  public login(username, password) {
    return this.http.post(
      `http://194.87.232.68:8081/api/users/login`,
      {
        email: username,
        password: password
      },
      {
        headers: {
          'Content-Type': 'application/json'
        },
        responseType: 'json',
        withCredentials: true
      }
    ).pipe(map(data => {
      console.log(data);
      const message = data['message'];
      if (message === 'Log in successful') {
        this.isAuthorize = true;
        this.errorMessage = message;
      }
      return this.isAuthorize;
    }));
  }
}
