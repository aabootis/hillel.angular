import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContactViewInstagramService {
  public list = [
    {
      id: 1,
      images: [
        'assets/img/1.jpeg',
        'assets/img/2.jpeg',
        'assets/img/3.jpeg'
      ]
    },
    {
      id: 2,
      images: [
        'assets/img/3.jpeg',
        'assets/img/1.jpeg',
        'assets/img/2.jpeg'
      ]
    },
    {
      id: 3,
      images: [
        'assets/img/1.jpeg',
        'assets/img/3.jpeg',
        'assets/img/2.jpeg'
      ]
    },
    {
      id: 4,
      images: [
        'assets/img/2.jpeg',
        'assets/img/3.jpeg',
        'assets/img/1.jpeg'
      ]
    }
  ];

  constructor() { }
}
