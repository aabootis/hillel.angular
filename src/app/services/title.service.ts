import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TitleService {
  public title = new Subject();

  constructor() { }

  // public title = of(this.titleValue);

  setTitle(value: string) {
    this.title.next(value);
  }
}
