import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {
  private zeroGroups = [
    {
      id: 'all',
      title: 'All contacts'
    }
  ];
  public listOfGroups: any[];

  constructor(private http: HttpClient) { }

  public getGroups() {
    return this.http.get(`http://194.87.232.68:8081/api/categories?`)
      .pipe(map(data => {
        const localData = data as any[];
        const serverGroups = localData.map(group => {
          return {
            id: group._id,
            title: group.name
          };
        });
        this.listOfGroups = [ ...this.zeroGroups, ...serverGroups ];
        return this.listOfGroups;
      }));
  }

  public get list() {
    return this.listOfGroups;
  }

  public getGroupById(id) {
    return this.listOfGroups.find(item => {
      return item.id === id;
    });
  }

}
