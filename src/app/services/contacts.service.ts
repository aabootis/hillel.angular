import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  public list = []
    /* {
      id: 1,
      name: 'Warhol',
      surname: 'Handy',
      description: 'Some text friends',
      phone: '',
      email: '',
      birthday: '',
      information: '',
      instagram: '',
      facebook: '',
      group: 1
    },
    {
      id: 2,
      name: 'Warhol',
      surname: 'Bandy',
      description: 'Some text family',
      phone: '',
      email: '',
      birthday: '20.05.2018',
      information: '',
      instagram: '',
      facebook: '',
      group: 2
    },
    {
      id: 3,
      name: 'Warhol',
      surname: 'Sandy',
      description: 'Some text family',
      phone: '',
      email: '',
      birthday: '20.05.2018',
      information: '',
      instagram: '',
      facebook: '',
      group: 3
    },
    {
      id: 4,
      name: 'Warhol ',
      surname: 'Dandy',
      description: 'Some text',
      phone: '',
      email: '',
      birthday: '',
      information: '',
      instagram: '',
      facebook: '',
      group: 2
    } */

  constructor(private http: HttpClient) { }

  get names() {
    return this.list.map(item => {
      return '' + item.name + ' ' + item.surname;
    });
  }

  getContactById(id: number) {
    return this.list.find(item => {
      return item.id === id;
    });
  }

  getContactsByGroupId(id: number): Observable<any[]> {
    return of(this.list.filter(item => {
      return item.category._id === id;
    }));
  }

  getContacts() {
    return this.http.get(`http://194.87.232.68:8081/api/phonebook?`,
      {
        withCredentials: true
      }).pipe(map((data: Array<any>) => {
        this.list = [ ...data ].filter(element => element.name);
        return this.list;
      }));
    // return of(this.list);
  }

  getBDays() {
    return this.list.filter(item => {
      return item.birthday;
    }).sort();
  }
}
