import { TestBed, inject } from '@angular/core/testing';

import { ContactViewInstagramService } from './contact-view-instagram.service';

describe('ContactViewInstagramService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContactViewInstagramService]
    });
  });

  it('should be created', inject([ContactViewInstagramService], (service: ContactViewInstagramService) => {
    expect(service).toBeTruthy();
  }));
});
