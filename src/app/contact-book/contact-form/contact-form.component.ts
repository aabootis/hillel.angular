import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { TitleService } from '@services/title.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  @Input() currentContact;
  public title = 'New contact';

  constructor(private location: Location,
    private titleService: TitleService) { }

  ngOnInit() {
    this.titleService.setTitle(this.title);
  }

  goBack(event) {
    event.preventDefault();
    this.location.back();
  }

}
