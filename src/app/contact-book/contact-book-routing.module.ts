import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactBookComponent } from './contact-book.component';
import { AuthGuard } from '../services/auth.guard';
import { ContactsComponent } from './contacts/contacts.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { ContactFullViewComponent } from './contact-full-view/contact-full-view.component';

const routes: Routes = [
  {
    path: '',
    component: ContactBookComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'contacts',
        component: ContactsComponent
      },
      {
        path: 'contacts/:id',
        component: ContactsComponent
      },
      {
        path: 'contact-form',
        component: ContactFormComponent
      },
      {
        path: 'contact-full-view',
        component: ContactFullViewComponent
      },
      {
        path: 'contact-full-view/:id',
        component: ContactFullViewComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactBookRoutingModule { }
