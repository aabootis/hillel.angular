import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ContactViewInstagramService } from '@services/contact-view-instagram.service';
import { ContactsService } from '@services/contacts.service';
import { TitleService } from '@services/title.service';
import { GroupsService } from '@services/groups.service';

@Component({
  selector: 'app-contact-full-view',
  templateUrl: './contact-full-view.component.html',
  styleUrls: ['./contact-full-view.component.scss']
})
export class ContactFullViewComponent implements OnInit {
  @Input() contact;
  public currentContactImages;
  public title = 'View contact';
  public currentGroupTitle;

  constructor(public instaImages: ContactViewInstagramService,
    private route: ActivatedRoute,
    private contacts: ContactsService,
    private location: Location,
    private titleService: TitleService,
    private groups: GroupsService) {}

  ngOnInit() {
    this.route.params
      .subscribe(params => {
        this.getContact(parseInt(params['id'], 10));
      });
  }

  close() {
    this.contact = undefined;
    this.location.back();
  }

  getContact(id: number): void {
    if (id) {
      this.contact = this.contacts.getContactById(id);
      this.currentContactImages = this.instaImages.list.find(elem => {
        return elem.id === this.contact.id;
      });
      this.titleService.setTitle(this.contact.name + ' ' + this.contact.surname);
      this.currentGroupTitle = this.groups.getGroupById(this.contact.group).title;
    }
  }
}
