import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactBookRoutingModule } from './contact-book-routing.module';
import { SidebarModule } from '../sidebar/sidebar.module';

import { ContactBookComponent } from './contact-book.component';
import { ContactsComponent } from './contacts/contacts.component';
import { ContactComponent } from './contact/contact.component';
import { ContactFullViewComponent } from './contact-full-view/contact-full-view.component';
import { ContactFormComponent } from './contact-form/contact-form.component';

@NgModule({
  imports: [
    CommonModule,
    ContactBookRoutingModule,
    SidebarModule
  ],
  declarations: [
    ContactBookComponent,
    ContactsComponent,
    ContactComponent,
    ContactFullViewComponent,
    ContactFormComponent
  ]
})
export class ContactBookModule { }
