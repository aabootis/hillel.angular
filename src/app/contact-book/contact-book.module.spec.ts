import { ContactBookModule } from './contact-book.module';

describe('ContactBookModule', () => {
  let contactBookModule: ContactBookModule;

  beforeEach(() => {
    contactBookModule = new ContactBookModule();
  });

  it('should create an instance', () => {
    expect(contactBookModule).toBeTruthy();
  });
});
