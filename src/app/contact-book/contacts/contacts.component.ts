import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContactsService } from '@services/contacts.service';
import { GroupsService } from '@services/groups.service';
import { TitleService } from '@services/title.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  public contactsList;
  public group;

  constructor(
    public contacts: ContactsService,
    private route: ActivatedRoute,
    private groups: GroupsService,
    private titleService: TitleService
  ) {}

  ngOnInit() {
    this.route.params
      .subscribe(params => {
        this.getcontactsList(params['id']);
      });
  }

  getcontactsList (id) {
    // const id = +this.route.snapshot.paramMap.get('id');
    if (id && id !== 'all') {
      this.contacts.getContactsByGroupId(id)
        .subscribe(contacts => {
          this.contactsList = contacts;
          this.titleService.setTitle(this.groups.getGroupById(id).title);
        });
    } else {
      this.contacts.getContacts()
        .subscribe(contacts => {
          this.contactsList = contacts;
          this.titleService.setTitle('All contacts');
        });
    }
  }

}
