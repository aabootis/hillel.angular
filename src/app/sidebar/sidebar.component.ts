import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public groupsHide = false;
  public bdaysHide = false;

  constructor(private route: Router) { }

  ngOnInit() {
  }

  gotoRoot() {
    this.route.navigateByUrl('/');
  }

}
