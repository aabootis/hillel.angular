import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BDayListComponent } from './b-day-list.component';

describe('BDayListComponent', () => {
  let component: BDayListComponent;
  let fixture: ComponentFixture<BDayListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BDayListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BDayListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
