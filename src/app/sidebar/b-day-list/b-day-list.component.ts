import { Component, OnInit } from '@angular/core';
import { ContactsService } from '../../services/contacts.service';

@Component({
  selector: 'app-b-day-list',
  templateUrl: './b-day-list.component.html',
  styleUrls: ['./b-day-list.component.scss']
})
export class BDayListComponent implements OnInit {
  public contacts;

  constructor(private contactsService: ContactsService) { }

  ngOnInit() {
    this.getListBDays();
  }

  getListBDays() {
    this.contacts = this.contactsService.getBDays();
  }

}
