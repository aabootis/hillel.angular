import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SidebarRoutingModule } from './sidebar-routing.module';

import { SidebarComponent } from './sidebar.component';
import { CreateContactComponent } from './create-contact/create-contact.component';
import { BDayListComponent } from './b-day-list/b-day-list.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { SearchComponent } from './search/search.component';
import { SideUserComponent } from './side-user/side-user.component';


@NgModule({
  imports: [
    CommonModule,
    SidebarRoutingModule
  ],
  exports: [
    SidebarComponent
  ],
  declarations: [
    SidebarComponent,
    CreateContactComponent,
    BDayListComponent,
    SideMenuComponent,
    SearchComponent,
    SideUserComponent
  ]
})
export class SidebarModule { }
