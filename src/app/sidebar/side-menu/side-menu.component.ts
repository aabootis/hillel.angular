import { Component, OnInit, OnDestroy } from '@angular/core';
import { GroupsService } from '../../services/groups.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit, OnDestroy {
  public groups = [];
  private subscriptions: Subscription[] = [];
  constructor(public groupsService: GroupsService) { }

  ngOnInit() {
    const subscription = this.groupsService.getGroups()
      .subscribe(groups => this.groups = groups);

    this.subscriptions.push(subscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
