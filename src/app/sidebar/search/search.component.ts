import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { ContactsService } from '../../services/contacts.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  public searchPhrase: FormControl;
  public filteredPhrase: Observable<any[]>;

  constructor(public contacts: ContactsService) {
    this.searchPhrase = new FormControl;

    this.filteredPhrase = this.searchPhrase.valueChanges
    .pipe(
      startWith(''),
      map(phrase => phrase ? this.filterPhrases(phrase) : this.contacts.names)
    );
  }

  ngOnInit() {
  }

  filterPhrases(phrase: string) {
    return this.contacts.names.filter(contact =>
      contact.toLowerCase().indexOf(phrase.toLowerCase()) === 0);
  }

}
