import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { LoginModule } from './login/login.module';
import { ContactBookModule } from './contact-book/contact-book.module';
import { AuthGuard } from './services/auth.guard';
import { AuthService } from './services/auth.service';
import { AppComponent } from './app.component';
import { GroupsService } from '@services/groups.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    LoginModule,
    ContactBookModule
  ],
  providers: [AuthGuard, AuthService, GroupsService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
