import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public error: string;

  constructor(public authService: AuthService,
    public router: Router) { }

  ngOnInit() {
    if (this.authService.isAuthorize) {
      let { url } = this.authService;
      url = url ? url : '/';

      this.router.navigate([url]);
    }
  }

  public login(user, password) {
    this.authService.login(user, password)
      .subscribe(isAuth => {
        if (isAuth) {
          let { url } = this.authService;
          url = url ? url : '/';
          this.router.navigate([url]);
        } else {
          this.error = this.authService.errorMessage;
        }
      },
      error => {
        this.error = error.statusText;
      });
  }

}
